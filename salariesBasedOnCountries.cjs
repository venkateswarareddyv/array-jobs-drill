let givendata = require('./1-arrays-jobs.cjs');

let toFindSalaryBasedOnCountries=givendata.reduce((initialObject,object)=>{
    let country=object.location;
    let salaries=Number((object.salary).replace("$",""))
    if(!initialObject[country]){
        initialObject[country]=salaries;
    }else{
        initialObject[country] +=salaries;
    }
    return initialObject;
},{});

console.log(toFindSalaryBasedOnCountries);