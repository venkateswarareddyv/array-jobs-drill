let givendata = require('./1-arrays-jobs.cjs');

let toFindSalaryBasedOnCountries=givendata.reduce((initialObject,object)=>{
    let country=object.location;
    let salaries=Number((object.salary).replace("$",""))
    if(!initialObject[country]){
        initialObject[country]={count:1,obtainedSalary:salaries};
    }else{
        initialObject[country].count +=1
        initialObject[country].obtainedSalary +=salaries
    }
    return initialObject;
},{});

let newObject={}

for(let key in toFindSalaryBasedOnCountries){
    let average=toFindSalaryBasedOnCountries[key].obtainedSalary/toFindSalaryBasedOnCountries[key].count;
    newObject[key]=average;
}

console.log(toFindSalaryBasedOnCountries)
console.log(newObject)